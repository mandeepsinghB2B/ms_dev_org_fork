public with sharing class CaseWrap {
       public Case c {get; set;}
       public String selVal {get; set;}
    
       public CaseWrap(Case c)
       {
          this.c = c;
       }
}