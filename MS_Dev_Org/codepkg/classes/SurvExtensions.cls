public class SurvExtensions{

      Public List<case> quest{get;set;}

      public SurvExtensions(ApexPages.StandardController controller) {
      
      Id id1 = UserInfo.getProfileId();

      String profileName = [Select Name from Profile where Id = :id1].Name ;
      if (profileName == 'System Administrator')
      {
        quest=[select CaseNumber , ClosedDate,CreatedDate from case ];    
      }
      else 
      {
      quest= [select CaseNumber, ClosedDate,CreatedDate
                from case where CreatedBy.Name =:UserInfo.getName()];
      }   
      }
}