public class LineItem_Controller {
  
    //*****Declare GLOBAL variables
    public Opportunity oppid {get; set;}
    private List <Opportunity> oppList = new List <Opportunity>();
    private List <OpportunityLineItem> prodList = new List <OpportunityLineItem>();
    private List <OpportunityLineItem> addRowList = [SELECT PricebookEntry.Product2.Name, quantity, unitprice, totalprice
                                                    FROM OpportunityLineItem
                                                    WHERE OpportunityID='0069000000WJPCF'];
    public OpportunityLineItem newrecord{get;set;}
    //********
  
     public LineItem_Controller(ApexPages.StandardController controller)
        {
        
        }
  
    //Get the Opportunity Detail Inforamation from the Opportunity Detail Page and return a list of that information.
    public List <Opportunity> getOpportunityInfo()
    {
    System.debug('Debug statement '+oppid.id);
        List <Opportunity> opp_InfoList = [SELECT name, ownerID, createddate, stagename, amount
                               FROM Opportunity
                               WHERE id = '0069000000WJPCF'];
        oppList = opp_InfoList;
  
    return opp_infoList;
    }
  
    //Get the Product Detail Information associated with the Opportunity. The WHERE queries whatever Opportunity ID is currently in context.
    public List <OpportunityLineItem> getProductInfo()
    {
        List <OpportunityLineItem> prod_InfoList = [SELECT PricebookEntry.Product2.Name, quantity, unitprice, totalprice
                                                    FROM OpportunityLineItem
                                                    WHERE OpportunityID='0069000000WJPCF'];
        prodList = prod_InfoList;
      
        return prod_InfoList;
    }
 
  
    //****SAVE AND CANCEL METHODS
    public PageReference cancel() {
       PageReference cancelReference = new PageReference('https://na15.salesforce.com/006i000000H8Abn');
        cancelReference.setRedirect(true);
        return cancelReference;
    }

    public PageReference save() {
        update prodList;
        update oppList;
        PageReference reference = new PageReference('/0069000000WJPCF');
        system.debug('Mandeep '+reference);
        reference.setRedirect(true);
        return reference;
    }
  
    //******
  
  //****ADD NEW ROW

   public void addRow()
   {
       addRowList.add(newrecord);
   }
}