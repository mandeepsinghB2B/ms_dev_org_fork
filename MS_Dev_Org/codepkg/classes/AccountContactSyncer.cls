public class AccountContactSyncer {

    public static final Integer BULK_CALL_SIZE = 5;
    @InvocableMethod
    public static void syncAddresses(List<Id> acctIds) {
        //TraceLog log =  new TraceLog('AccountContactSync');
        String traceStr = 'syncAddresses called. acctIds: ' + acctIds + '\n';
        System.debug('in syncAddresses. acctIds: ' + acctIds);
        //String grpDevName = AccountUtils.getGroupL1DevName();
        List<Contact> contacts = null;
        List<Contact> updateList = new List<Contact>();
        try {
            // Filter - branch and customer levels only (not group level)
            List <Account> accts = [SELECT Id,Name,ParentId,
                                    BillingAddress,BillingCity,BillingCountry,
                                    BillingLatitude,BillingLongitude,
                                    BillingPostalCode,BillingState,BillingStreet,
                                    BillingStateCode, BillingCountryCode,
                                    IsDeleted                                    
                                    FROM Account
                                    WHERE IsDeleted = false                                    
                                    AND Id in :acctIds];
          System.debug('in syncAddresses. accts: ' + accts);
            Map<Id, Account> acctMap = new Map<Id, Account>(accts);
            Set<Id> idSet = acctMap.keySet(); // this set is more filtered than acctIds.
          System.debug('in syncAddresses. idSet: ' + idSet);
    
            // Contact_Type__c is null or 'Standard Contact'. How do we filter out Marketing?
            // What is the "Receive mail at branch checkbox"? Is it Not_Onsite__c ? 
            // On 6/10/2015 - Removed MailingAddress from query - No longer found at run time (compiles)!
            if (idSet.size() > 0) {
              contacts = [SELECT AccountId,Id,
                                      MailingCity,MailingCountry,
                                      MailingLatitude,MailingLongitude,
                                      MailingPostalCode,
                                      MailingState,
                                      MailingStateCode, MailingCountryCode,
                                      MailingStreet
                                      FROM Contact where IsDeleted = false and
                                      AccountId in :idSet];
            } else {
                contacts = new List<Contact>();
            }
          System.debug('in syncAddresses. contacts: ' + contacts);
            for (Contact c : contacts) {
                Account a = acctMap.get(c.AccountId);
                if (areAddressesDifferent(a, c)) {
              System.debug('in syncAddresses. syncContact: ' + a + ' contact: ' + c);
                    syncContact(a, c);
                    updateList.add(c);
                }
            }
        } catch (Exception e) {
            System.debug('in syncAddresses. syncContact: ' + e.getMessage());
            traceStr += 'Exception encountered: ' + e.getMessage() + '\n';
        }
        if (updateList.size() > 0) {
            boolean groupingDone = false;
            while (!groupingDone && updateList.size() > 0) {
                if (updateList.size() <= BULK_CALL_SIZE) {
                    traceStr += 'Updating: ' + updateList + '\n';
                    update updateList;
                    groupingDone = true;
                } else {
                    List<Id> updateContactIdListSubset = new List<Id>();
                    List<Id> updateAccountIdListSubset = new List<Id>();
                    for (integer looper = 0; looper < BULK_CALL_SIZE; looper++) {
                        Contact c = updateList.remove(0);
                        updateContactIdListSubset.add(c.Id);
                        updateAccountIdListSubset.add(c.AccountId);
                    }
                    SyncContactAddressesInFuture(updateContactIdListSubset, updateAccountIdListSubset);
                }
            }
        }
        
    }
    
    public static Boolean areAddressesDifferent(Account a, Contact c) {
        Boolean retVal = false;
        //c.MailingAddress = a.BillingAddress
        //
        if (c.MailingStateCode != a.BillingStateCode ||
            c.MailingCountryCode != a.BillingCountryCode ||
            c.MailingCity != a.BillingCity ||
          c.MailingCountry != a.BillingCountry ||
          c.MailingLatitude != a.BillingLatitude ||
          c.MailingLongitude != a.BillingLongitude ||
          c.MailingPostalCode != a.BillingPostalCode ||
          c.MailingState != a.BillingState ||
            c.MailingStreet != a.BillingStreet) {
                retVal = true;
            }
        return retVal;
    }
    
    public static void syncContact(Account a, Contact c) {
        //c.MailingAddress = a.BillingAddress
        //
    System.debug('State Code... Contact: ' + c.MailingStateCode + ' Acct: ' + a.BillingStateCode);
    System.debug('zip Code... Contact: ' + c.MailingPostalCode + ' Acct: ' + a.BillingPostalCode);
    System.debug('Street... Contact: ' + c.MailingStreet + ' Acct: ' + a.BillingStreet);
        c.MailingStateCode = a.BillingStateCode;
        c.MailingCountryCode = a.BillingCountryCode;
        c.MailingCity = a.BillingCity;
        c.MailingCountry = a.BillingCountry;
        c.MailingLatitude = a.BillingLatitude;
        c.MailingLongitude = a.BillingLongitude;
        c.MailingPostalCode = a.BillingPostalCode;
        c.MailingState = a.BillingState;
        c.MailingStreet = a.BillingStreet;
    }
    @future
    public static void SyncContactAddressesInFuture(List<Id> updateContactIdListSubset, List<Id> updateAccountIdListSubset) {
        if (updateContactIdListSubset.size() > 0) {
          //TraceLog log =  new TraceLog('AccountContactSync');
           String traceStr = 'SyncContactAddressesInFuture called with contactIds: ' + updateContactIdListSubset +
                  '\nand associated accountIds: ' + updateAccountIdListSubset + '\n';
            try {
                List<contact> contacts = [SELECT AccountId,Id,
                                          MailingCity,MailingCountry,
                                          MailingLatitude,MailingLongitude,
                                          MailingPostalCode,
                                          MailingState,
                                          MailingStateCode, MailingCountryCode,
                                          MailingStreet
                                          FROM Contact where IsDeleted = false and
                                          Id in :updateContactIdListSubset];
                List <Account> accts = [SELECT Id,Name,ParentId,
                                        BillingAddress,BillingCity,BillingCountry,
                                        BillingLatitude,BillingLongitude,
                                        BillingPostalCode,BillingState,BillingStreet,
                                        BillingStateCode, BillingCountryCode,                                        
                                        IsDeleted
                                        FROM Account
                                        WHERE IsDeleted = false
                                        AND Id in :updateAccountIdListSubset];
                System.debug('in SyncContactAddressesInFuture. accts: ' + accts);
                Map<Id, Account> acctMap = new Map<Id, Account>(accts);
                List<Contact> updateList = new List<Contact>();
                for (Contact c : contacts) {
                    Account a = acctMap.get(c.AccountId);
                    if (areAddressesDifferent(a, c)) {
                        System.debug('in SyncContactAddressesInFuture. syncContact: ' + a + ' contact: ' + c);
                        syncContact(a, c);
                        updateList.add(c);
                    }
                }
                update updateList;
            } catch (Exception e) {
                System.debug('in SyncContactAddressesInFuture. Exception: ' + e.getMessage());
                traceStr += 'SyncContactAddressesInFuture Exception: ' + e.getMessage();
            }
           
        }
    }

}