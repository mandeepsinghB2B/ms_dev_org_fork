public without sharing class Case_Approval_Test {
    
    public Id  aId                                                  {get;set;}
    public String stage{get;set;}
    
    public Case_Approval_Test ( ApexPages.StandardController stdController ) {
        try { 
            aId = ApexPages.currentPage().getParameters().get('id');

        }catch( Exception e ) { 
            ApexPages.addMessage( new ApexPages.message(ApexPages.severity.ERROR, 'Error occured while loading page. ' + e.getMessage() ) );  
        }
    }
    
    
    public void submitForApproval(){
        PageReference pr=Page.Case_Approval_Test;
        pr.getParameters().put('id',aId );
        pr.setRedirect( true );
        
        Approval.ProcessSubmitRequest agreementApprovalProcess = new Approval.ProcessSubmitRequest();
        agreementApprovalProcess.setComments('asas');
        agreementApprovalProcess.setObjectId(aId);
        agreementApprovalProcess.setNextApproverIds(new Id[] {UserInfo.getUserId()});

      
        Approval.ProcessResult result = Approval.process(agreementApprovalProcess);
        
        //return pr;
       
    }
    

}