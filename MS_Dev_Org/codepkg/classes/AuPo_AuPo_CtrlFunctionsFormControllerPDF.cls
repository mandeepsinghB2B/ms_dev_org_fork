public class AuPo_AuPo_CtrlFunctionsFormControllerPDF {

public integer CFCount {get;set;}       // count of approved CF involvements at this firm
public integer wCFCount {get;set;}      // count of withdrawn CF wrappers in this firm wrapper

public AuPo_AuPo_CtrlFunctionsFormControllerPDF()
{
CFCount = 4;
wCFCount = 3;
}

}