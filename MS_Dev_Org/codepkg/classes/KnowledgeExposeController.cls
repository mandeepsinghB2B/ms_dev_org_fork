@RestResource(urlMapping='/KnowledgeExpose/*')
global with sharing class KnowledgeExposeController
{
    @HttpGet
    global static List<Q_A__kav> doGet() 
    {
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;        
        //String datacategory= req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        //Q_A__kav result = [SELECT Id, Title, Answer__c,LastModifiedDate FROM Q_A__kav WHERE language=:languagecode2 and createdDate > LAST_WEEK and PublishStatus ='Online'];
        
        String languagecode= req.params.get('lang');
        
        if(languagecode==null)
        {
            languagecode= 'en_US';
        }
        String status = 'Online';
        String datacategory= 'UK__c';
        
        // WITH DATA CATEGORY region__c AT datacategory
        //List<Q_A__kav> result = Database.query('SELECT Id, Title, Answer__c, Internal_Links__c,ECom_EMEA_Attachment__Body__s FROM Q_A__kav WHERE language=:languagecode and createdDate > LAST_WEEK and PublishStatus =:status');          List<Q_A__kav> result = D
        List<Q_A__kav> result = [SELECT Id, Title, AppSFD__Internal_Links__c ,  AppSFD__ECom_EMEA_Attachment__Body__s FROM AppSFD__Q_A__kav WHERE language = 'en_US' and PublishStatus ='Draft'];
  
        return result;
    }
    
    @HttpPost
    global static String doPost(String title,String answer) 
    {
        Q_A__kav kav= new Q_A__kav();
        
        kav.Title = title;
        kav.URLName = title;
        kav.Summary = title;
       // kav.Answer__c = answer;
        

        insert kav;
        return kav.Id;
    }
}