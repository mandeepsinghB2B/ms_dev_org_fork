trigger AccountOwnerContactOwnerSync on Account (before update) {
  
  //First things first, we need to make a list of the accountIds
  //to send to the trigger
   Set<Id> accountIds = Trigger.newMap.keyset();

   //we create a list of all accounts where the Id is equal to the 
   //Id of the accounts we send to the trigger
   List<Account> theAccounts = [SELECT Id, OwnerId, (select ID, AccountID from Contacts)
         FROM Account
         WHERE Id IN :accountIds];

   // create the list of contacts to update AKA "theContacts"
   List<Contact> theContacts = new List<Contact>(); 
   
   //The owner needs to have changed for us to want to update so...
   //Check the accouonts to see if the ownerId changes  
   
   //here is where we loop over the accounts
      for (Account a : theAccounts){

         //and here is where we check the values 
         String oldOwnerIds = Trigger.oldMap.get(a.Id).OwnerId;
         String newOwnerIds = a.OwnerId;

         //If the owner is new, create a list of the contacts on said account
         if(oldOwnerIds != newOwnerIds)
         {
            for(Contact c: a.Contacts){
                if(c.OwnerId != newOwnerIds){
                    c.OwnerId = a.OwnerId;
                }
            }
         }
      }
   }