trigger Case_owner_update on Case (before insert, before update) 
{
system.debug('##### Keyset '+Trigger.newMap.keySet());
    for (Case casesobj: Trigger.new)
    {
      //Field that you want to be copied by other field keep those on left side
      if (casesobj.State__c != null )
      {
           if (casesobj.State__c == 'TX')
           {
               casesobj.tax__c = casesobj.Sub_total__c * 1.5;
           }
           if (casesobj.State__c == 'MX')
           {
               casesobj.tax__c = casesobj.Sub_total__c * 2.5;
           }
       }
       else
       {
           casesobj.tax__c = casesobj.Sub_total__c ;
       }    
    } 
}