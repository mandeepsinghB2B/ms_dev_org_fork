Trigger EventTrigger on Event (after delete, after insert, after undelete, after update, before insert, before update) 
{       
if(Trigger.isBefore) EventTriggerHandler.onBefore(Trigger.oldMap, Trigger.new);

}