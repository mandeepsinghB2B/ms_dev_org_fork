Trigger PopulateAddress_FromContact on Event (before update, before insert)
{

    /*    System.debug('Testttt@@@@@@');

        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        //String emailAddr = 'Mandeep.singh@salesforce.com';
        String emailAddr = 'mandeep_s23@yahoo.in';
        String newOwnerName = 'Mandeep Singh';

        String[] toAddresses = new String[] {emailAddr};
        mail.setToAddresses(toAddresses);

        mail.setSubject('Owner Changed for Account : ');

        mail.setPlainTextBody('Owner of Account: ' + 'Admin User' + ' Changed to ' + newOwnerName);
        
        mail.setPlainTextBody('Description: ' + trigger.new[0].description);
        
        //mail.setHtmlBody('Owner of Account: <b>' +trigger.new[0].description + ' Shanu ' + '</b> Changed to <b>' + newOwnerName  + '</b>');

        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        */

Set<Id> cs = new Set<ID>();
 
for(Event t: trigger.New)
{
cs.add(t.WhoID);
}
//System.Debug('****CASEID'+cs );
 
Map<ID,Contact> cse = new Map<ID,Contact>([Select ID,MailingCity,MailingCountry,MailingLatitude,MailingLongitude,MailingPostalCode,MailingState,MailingStreet From Contact Where ID IN:cs]);
//System.Debug('****'+cse);
 
for(Event  t: trigger.New)
{
String address = 'City :' + ' '+ cse.get(t.WhoID).MailingCity +  'Country :' + ' '+ cse.get(t.WhoID).MailingCountry ;
t.Contact_Address_Pop__c = address ;
}
}